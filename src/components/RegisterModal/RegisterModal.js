import React, { Component } from 'react';
import './RegisterModal.css';
import { API_URL } from '../../helpers/globalVars';

class RegisterModal extends Component {
    defaultSubmitText = 'Avengers, assemble!';

    state = {
        isSubmitingForm: false,
        titleText: 'Regístrate ahora, ¡es gratis!',
        titleBackground: '#ffd700',
        submitText: this.defaultSubmitText,
        errors: {}
    }

    handleFormSubmit = (e) => {
        e.preventDefault();
        this.setState({ isSubmitingForm: true })

        const formData = new FormData(e.target);

        fetch(`${API_URL}register`, { method: "POST", body: formData})
            .then(res => res.json())
            .then(data => {
                console.log(data);
                if (data.error) {
                    this.setState({ 
                        titleText: typeof data.data === 'object' ? 'Múltiples errores' : data.data,
                        errors: data.data,
                        titleBackground: 'coral'
                    })
                } else {
                    this.setState({ submitText: '¡Bienvenido a Moobee!' })
                    setTimeout(() => {
                        this.props.closeModal();
                    }, 1000)
                }
                this.setState({ isSubmitingForm: false })
            })

    }

    render() {
        return (
            <div className="modal">
                <div className="title" style={{ background: this.state.titleBackground }}>
                    <p>{this.state.titleText}</p>
                </div>
                <div className="body">
                    <form onSubmit={this.handleFormSubmit}>
                        <div className="input-group">
                            <label htmlFor="first_name">Nombre</label>
                            {this.state.errors.first_name ? <label className="error" htmlFor="first_name">{this.state.errors.first_name[0]}</label> : null }
                            <input className={this.state.errors.first_name ? "error" : null} type="text" name="first_name" placeholder="Tony Stark" />
                        </div>
                        <div className="input-group">
                            <label htmlFor="username">Nombre de usuario</label>
                            {this.state.errors.username ? <label className="error" htmlFor="username">{this.state.errors.username[0]}</label> : null }
                            <input className={this.state.errors.username ? "error" : null} type="text" name="username" placeholder="ironman" />
                        </div>
                        <div className="input-group">
                            <label htmlFor="email">Email</label>
                            {this.state.errors.email ? <label className="error" htmlFor="email">{this.state.errors.email[0]}</label> : null }
                            <input className={this.state.errors.email ? "error" : null} type="email" name="email" placeholder="ceo@stark.co" />
                        </div>
                        <div className="input-group">
                            <label htmlFor="password">Contraseña</label>
                            {this.state.errors.password ? <label className="error" htmlFor="password">{this.state.errors.password[0]}</label> : null }
                            <input className={this.state.errors.password ? "error" : null} type="password" name="password" placeholder="hottestguy3000" />
                        </div>
                        <div className="input-group">
                            <input className={this.state.isSubmitingForm ? 'loading' : null} type="submit" value={this.state.submitText} />
                        </div>

                        <input type="hidden" name="source" value="web" />

                    </form>
                </div>
            </div>
        );
    }
}

export default RegisterModal;