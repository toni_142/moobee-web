import React from 'react';
import ReactDOM from 'react-dom';
import RegisterModal from '../RegisterModal/RegisterModal';
import './Modal.css';

const Modal = ({ onClose, open }) => open ? 
    ReactDOM.createPortal(<RegisterModal closeModal={onClose} />, document.querySelector('#modal')) :
    null;

export default Modal;