import React, { Component } from 'react';
import './Watchlist.css';
import Badge from '../../resources/images/badge.svg';
import Photo from '../../resources/images/watchlist.jpg';

class Watchlist extends Component {
    handleRegisterClick = (e) => {

    } 

    render() {
        return (
            <div className="watchlist">
                <div className="text">
                    <h2>
                        Una base de datos infinita
                    </h2>
                    <p className="desktop-only">
                    Si lo has visto, está en Moobee. Usamos la base de datos de
                    TheMovieDB, por lo que tendrás acceso a los datos de todas
                    las películas que te imaginas y las que no sabías ni que existían.
                    También podrás buscar y descubrir actores y directores, así
                    nunca te perderás una de sus nuevas películas.                    
                    </p>
                </div>
                <div className="image">
                    <img src={Photo} alt="Captura Moobee APP"/>
                </div>
                <div className="text full-width">
                    <h2>Y lo más importante, ¡no estás solo!</h2>
                    { this.props.usersRegistered ? (<h3 className="highlight">¡{this.props.usersRegistered} usuarios registrados!</h3>) : null}
                    <h2 onClick={this.props.toggleModal} style={{ textDecoration: 'underline', cursor: 'pointer' }}>¡Únete a la comunidad Moobee!</h2>                  
                </div>
                <div className="button">
                    <a href="https://apple.es" target="_blank">
                        <img src={Badge} alt="Descarga la APP en la APP STORE" />
                    </a>
                </div>
            </div>
        );
    }
}

export default Watchlist;