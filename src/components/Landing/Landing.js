import React, { Component } from 'react';
import logo from '../../resources/images/logo.png';
import Hero from '../Hero/Hero';
import Seguimiento from '../Seguimiento/Seguimiento';
import Moobs from '../Moobs/Moobs';
import Watchlist from '../Watchlist/Watchlist';
import { API_URL } from '../../helpers/globalVars';

class Landing extends Component {
    state = {
        moviesSeen: null,
        moobsSent: null,
        usersRegistered: null,
    };

    componentWillMount = () => {
        fetch(`${API_URL}home-info`)
            .then(res => res.json())
            .then(data => {
                this.setState({ 
                    moviesSeen: data.data.movies_seen,
                    moobsSent: data.data.moobs_sent,
                    usersRegistered: data.data.users_registered
                })
            })
    }

    render() {
        return (
            <div className="App">
                <header id="navbar">
                    <img src={logo} className="App-logo" alt="logo" />
                    <h1>Moobee</h1>
                </header>
                <Hero />
                <Seguimiento moviesSeen={this.state.moviesSeen} />
                <Moobs moobsSent={this.state.moobsSent} />
                <Watchlist toggleModal={this.props.toggleModal} usersRegistered={this.state.usersRegistered} />
            </div>
        );
    }
}

export default Landing;