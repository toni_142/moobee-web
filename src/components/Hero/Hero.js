import React, {
    Component
} from 'react';
import './Hero.css';
import Slider from '../Slider/Slider'

class Hero extends Component {
    state = {
        images: {
            "slide-1": -25,
            "slide-2": 30,
            "slide-3": 5,
            "slide-4": -15,
            "slide-5": 36,
            "slide-6": 10,
            "slide-7": 23,
            "slide-8": -5,
        }
    }
    render() {
        return ( 
            <div className="container">
                <div className="text">
                    <h1>¡Una red social de cine!</h1>
                    <p>Crea reviews de películas, guárdalas en listas y comparte todo con tus amigos</p>
                </div>
                <Slider images={this.state.images} />
            </div>
        );
    }
}

export default Hero;