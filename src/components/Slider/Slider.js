import React, { Component } from 'react';
import './Slider.css';

class Slider extends Component {
    renderImages = (images) => {
        return Object.keys(images).map((image, index) => {
            return (
                <div className="slide">
                    <img data-id={index} src={`/resources/images/${image}.png`} style={{ transform: `rotate(${images[image]}deg)` }} key={index} alt="Captura APP Moobee" />
                </div>
            )
        })
    }

    render() {
        return (
            <div className="slider">
                <div className="slide-track">
                    { this.renderImages(this.props.images) }     
                    { this.renderImages(this.props.images) }     
                </div>   
            </div>
        );
    }
}

export default Slider;