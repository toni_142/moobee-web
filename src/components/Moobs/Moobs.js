import React, { Component } from 'react';
import './Moobs.css';
import FirstImage from '../../resources/images/moobs-1.jpg';
import SecondImage from '../../resources/images/moobs-2.jpg';

class Moobs extends Component {
    render() {
        return (
            <div className="moobs">
                <div className="text">
                    <h2>
                        Crea moobs para compartir tu opinión
                    </h2>
                    <p className="desktop-only">
                    Moobee es una red social, así que no te olvides de la parte más importante, ¡crea una review de cada película que veas para que todos tus amigos las lean!
                    </p>
                    <p className="desktop-only">
                        Y no te preocupes, si tus amigos envían un moob de una película que no has visto, Moobee te avisará antes de enseñarte cualquier texto. ¡Adiós a los spoilers!
                    </p>
                </div>
                <div className="images">
                    <div className="image">
                        <img src={FirstImage} alt="Vista previa de un moob"/>
                    </div>
                    <div className="image">
                        <img src={SecondImage} alt="Vista previa de un moob"/>
                    
                    </div>
                </div>
                <div className="text">
                { this.props.moobsSent ? (<h3 className="highlight">¡{this.props.moobsSent} moobs enviados!</h3>) : null}
                </div>
            </div>
        );
    }
}

export default Moobs;